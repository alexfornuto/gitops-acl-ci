# GitLab CI to Sync Tailscale ACLs

This CI template lets you manage your [tailnet policy file](https://tailscale.com/kb/1018/acls/) using a
[GitOps](https://about.gitlab.com/topics/gitops/) workflow. With this template you can automatically
manage your tailnet policy file using a git repository as your source of truth. 

## Inputs

### `tailnet`

**Required** The name of your tailnet. You can find it by opening [the admin
panel](https://login.tailscale.com/admin) and copying down the name next to the
Tailscale logo in the upper left hand corner of the page.

### `api-key`

**Required** An API key authorized for your tailnet. You can get one [in the
admin panel](https://login.tailscale.com/admin/settings/keys).

Please note that API keys will expire in 90 days. Set up a monthly event to
rotate your Tailscale API key.

### `policy-file`

**Optional** The path to your policy file in the repository. If not set this
defaults to `policy.hujson` in the root of your repository.


## Getting Started

Set up a new GitHub repository that will contain your tailnet policy file. Open the [Access Controls page of the admin console](https://login.tailscale.com/admin/acls) and copy your policy file to
a file in that repo called `policy.hujson`.

If you want to change this name to something else, you will need to add the
`policy-file` argument to the `with` blocks in your GitHub Actions config.

Copy this file to `.gitlab-ci.yml`.

```yaml
include:
  - project: 'tailscale-dev/gitops-acl-ci'
    ref: main
    file: 'acls.gitlab-ci.yaml'
    with:
      api-key: $TS_API_KEY
      tailnet: $TS_TAILNET

stages:
  - test
  - apply

test:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

apply:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
```

Generate a new API key [here](https://login.tailscale.com/admin/settings/keys).

Set a monthly calendar reminder to renew this key because Tailscale does not
currently support API key renewal (this will be updated to support that when
that feature is implemented).

Then open the secrets settings for your repo and add two secrets:

* `TS_API_KEY`: Your Tailscale API key from the earlier step
* `TS_TAILNET`: Your tailnet's name (it's next to the logo on the upper
  left-hand corner of the [admin
  panel](https://login.tailscale.com/admin/machines))

Once you do that, commit the changes and push them to GitLab. You will have CI
automatically test and push changes to your tailnet policy file to Tailscale.
